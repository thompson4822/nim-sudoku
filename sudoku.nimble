task build, "Builds the program":
  setCommand "c"

task tests, "Tests the project":
    exec "nim c -r --path:src tests/all"

task release, "Build the project in release mode":
    exec "nim c -d:release src/main"

# Package

version       = "0.1.0"
author        = "Steve Thompson"
description   = "Sudoku, based on the earlier project in Dart called Ultimate Sudoku"
license       = "MIT"
srcDir        = "src"
bin           = @["main"]

# Deps

requires "nim >= 0.16.0"
#requires "einheit >= 0.1.6"
