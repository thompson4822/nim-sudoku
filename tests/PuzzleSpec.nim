import unittest, "Puzzle", sequtils, algorithm

proc asEmpty(g: Grid): Grid = 
    (grid: newSeqWith(9, newSeq[int](9)))

suite "Grid specification":
    test "when a puzzle is empty, any values can go in its first cell":
        let g = newGrid().asEmpty
        check(g.available((row: 0, col: 0)) == @[1, 2, 3, 4, 5, 6, 7, 8, 9])

    test "when a puzzle has a 1, 2, 3 down the first column, only 4-9 can go into the second cell":
        var g = newGrid().asEmpty
        g.grid[0][0] = 1; g.grid[1][0] = 2; g.grid[2][0] = 3
        check(g.available((row: 0, col: 1)) == @[4, 5, 6, 7, 8, 9])

    test "same setup as before, but values for row should be 1":
        var g = newGrid().asEmpty
        g.grid[0][0] = 1; g.grid[1][0] = 2; g.grid[2][0] = 3
        check(g.valuesForRow(0) == @[1])

    test "same setup as before, but values for col should be 1, 2, 3":
        var g = newGrid().asEmpty
        g.grid[0][0] = 1; g.grid[1][0] = 2; g.grid[2][0] = 3
        check(g.valuesForCol(0) == @[1, 2, 3])
    
    test "if top subgrid columns filled, some other cell in that subgrid will indicate those values used.":
        var g = newGrid().asEmpty
        g.grid[0][3] = 1; g.grid[0][4] = 2; g.grid[0][5] = 3
        check(g.valuesForSubgrid((row: 2, col: 4)) == @[1, 2, 3])
    
    test "same setup as above, but testing the validity of a number in use should yield false":
        var g = newGrid().asEmpty
        g.grid[0][3] = 1; g.grid[0][4] = 2; g.grid[0][5] = 3
        check(g.valid(1, (row: 2, col: 4)) == false)

    test "same setup as above, but testing the validity of a number not in use should yield true":
        var g = newGrid().asEmpty
        g.grid[0][3] = 1; g.grid[0][4] = 2; g.grid[0][5] = 3
        check(g.valid(5, (row: 2, col: 4)) == true)

    test "setting a value at a location actually does just that":
        var g = newGrid().asEmpty
        let location = (3, 8)
        g.set(9, location)
        check(g.grid[3][8] == 9)

    test "is last location returns false if the given location is not the last":
        let location = (0, 0)
        let g = newGrid()
        check(g.isLastLocation(location) == false)

    test "is last location returns true if the given location is the last":
        let location = (8, 8)
        let g = newGrid()
        check(g.isLastLocation(location) == true)
        
    test "nextLocation finds the next column if not at the end of the row":
        let location = (1, 0)
        let g = newGrid()
        check(g.nextLocation(location) == (1, 1))

    test "nextLocation finds the beginning of the next row if at the end of the row":
        let location = (1, 8)
        let g = newGrid()
        check(g.nextLocation(location) == (2, 0))

    test "a fully populated Grid should have no empty spaces":
        let g = newGrid()
        for row in 0 ..< g.grid.len:
            for col in 0 ..< g.grid[0].len:
                check(g.grid[row][col] != 0)

    test "a fully populated Grid is well formed":
        let g = newGrid()
        # This may look a little funky, but it is just checking diagonally. This is a more efficient way of checking every
        # cell than looping over every row and column.
        for rowCol in 0 ..< g.grid.len:
            # LANGUAGE NOTE - Nim uses in place sorting, which I can perfectly understand as the focus is on efficiency.
            # also note that by default the values will be Ascending, but you can specify Descending after the cmp if desirable.
            var colValues = g.valuesForCol(rowCol); colValues.sort(system.cmp)
            var rowValues = g.valuesForRow(rowCol); rowValues.sort(system.cmp)
            var subgridValues = g.valuesForSubgrid((rowCol, rowCol)); subgridValues.sort(system.cmp)
            check(colValues == rowValues and colValues == subgridValues)


#method populateGrid*(p: Grid, location: Location): bool {.base.} =
        
    