import sequtils
import random

randomize()

# -----------------------------------------------------------------------------------
# For Location
# -----------------------------------------------------------------------------------
type
    Location* = tuple[row, col: int]

proc `$`*(location:Location): string =
    "(" & $location.row & "/" & $location.col & ")"

# -----------------------------------------------------------------------------------
# For Grid
# -----------------------------------------------------------------------------------

# Questions about this type
# Alright, this looks like it is basically a tuple. If you were to take out the populateGrid call, you
# wouldn't even need a constructor. The reason all of this seems important is because printing a solution
# is what I would want to do in the case of either the Solution, or the Puzzle. So if there were something
# a little more generic to call it, like Grid, that would be great. As an added bonus, the methods that I
# would want to use, specifically 'available', would be naturally accessible. 
type
    Grid* = tuple[grid: seq[seq[int]]]

proc `$`*(g: Grid): string = 
    # Create textual representation of grid
    result = ""
    for row in 0 ..< g.grid.len:
        for col in 0 ..< g.grid[0].len:
            if col mod 3 == 0:
                result = result & "|"
            result = result & $g.grid[row][col] & " "
        result = result & "\n"

# Is the given value for the given row valid? 
proc valuesForRow*(g: Grid, row: int): seq[int] =
    result = @[]
    for col in 0 ..< 9:
        let value = g.grid[row][col]
        if value != 0: result.add(value)

# Is the given value for the given col valid? 
proc valuesForCol*(g: Grid, col: int): seq[int] =
    result = @[]
    for row in 0 ..< 9:
        let value = g.grid[row][col]
        if value != 0: result.add(value)

# Is the given value, at the given location, valid for the subgrid?
proc valuesForSubgrid*(g: Grid, location: Location): seq[int] =
    result = @[]
    # LANGUAGE NOTE - You must use int() if all you want is the int part. toInt will round (counterintuitively)
    var startRow = int(location.row / 3) * 3
    var startCol = int(location.col / 3) * 3
    for row in startRow ..< (startRow + 3):
        for col in startCol ..< (startCol + 3):
            let value = g.grid[row][col]
            if value != 0: result.add(value)

# I don't think this is really necessary ...
const ValidValues = @[1, 2, 3, 4, 5, 6, 7, 8, 9]

# What values are available for the given cell?
proc available*(g: Grid, location: Location): seq[int] = 
    let used = concat(g.valuesForRow(location.row), g.valuesForCol(location.col), g.valuesForSubgrid(location)).deduplicate
    ValidValues.filter(proc (x: int): bool = used.contains(x) == false)

# Is the given value at the given location valid?
proc valid*(g: Grid, value: int, location: Location): bool =
    g.available(location).contains(value)

proc set*(g: var Grid, value: int, location: Location) =
    g.grid[location.row][location.col] = value

proc get*(g: var Grid, location: Location): int =
    g.grid[location.row][location.col]

proc isLastLocation*(g: Grid, location: Location): bool =
    location.row == g.grid.len - 1 and location.col == g.grid[0].len - 1

proc nextLocation*(g: Grid, location: Location): Location =
    let (row, col) = location
    var newCol = if col + 1 == g.grid[0].len: 0 else: col + 1
    var newRow = if newCol < col: min(row + 1, g.grid.len - 1) else: row
    result = (row: newRow, col: newCol)

proc populateGrid*(g: var Grid, location: Location): bool =
    # Find out what values are allowable
    var available = g.available(location)
    result = false
    while result == false and available.len != 0:
        var index = random(available.len)
        g.set(available[index], location)
        available.delete(index)
        if g.isLastLocation(location):
            result = true
        else:
            result = g.populateGrid(g.nextLocation(location))
    if result == false:
        g.set(0, location)


proc newGrid*(): Grid =
    # Create an empty grid. The sequence should be filled with zeros
    # Create random numbers between 1 and 9 at each consecutive cell
    # while valid
    result = (grid: newSeqWith(9, newSeq[int](9)))
    discard result.populateGrid((0, 0))

# -----------------------------------------------------------------------------------
# For Puzzle
# -----------------------------------------------------------------------------------
type
    Difficulty* {.pure.} = enum
        Easy = 44,
        Medium = 48,
        Difficult = 52,
        Nightmare = 58

    Puzzle* = ref object of RootObj
        grid*, puzzle*: Grid
        emptiedLocations*: seq[Location] 
        difficulty*: Difficulty

proc `$`*(p: Puzzle): string = $p.puzzle

method canBeSolved(p: Puzzle, emptiedLocations: seq[Location]): bool {.base.} =
    if emptiedLocations.len == 0:
        true
    else: 
        var emptied = emptiedLocations
        for location in emptiedLocations:
            let available = p.puzzle.available(location)
            if available.len == 1:
                p.puzzle.set(available[0], location)
                emptied.keepIf(proc(loc: Location): bool = loc != location)
        if emptied.len == emptiedLocations.len and emptiedLocations.len != 0:
            # No cells solved, so invalid puzzle
            false
        else:
            p.canBeSolved(emptied)

method candidateLocation(p: Puzzle, emptied, discarded: seq[Location]): Location {.base.} =
    var proposed: Location = (random(9), random(9))
    while emptied.contains(proposed) or discarded.contains(proposed):
        proposed = (random(9), random(9))
    proposed

method clearPuzzleCells(p: var Puzzle, cells: seq[Location]) {.base.} =
    # clear all locations that are part of the puzzle
    for location in cells: p.puzzle.set(0, location)


method generate*(p: var Puzzle) {.base.} =
    # What are the cells that we've emptied? Saves us from having to scan the grid for empty cells
    var emptied: seq[Location] = @[]
    # What are the cells we've tried unsuccessfully to empty? Saves us from checking them again and again
    var discarded: seq[Location] = @[]
    while emptied.len < ord(p.difficulty):
        let candidate = p.candidateLocation(emptied, discarded)
        # Add candidates to the emptied list
        emptied = emptied & candidate
        p.clearPuzzleCells(emptied)
        if p.canBeSolved(emptied) == false:
            # If first and second created a puzzle that can't be solved, get rid of them.
            discard emptied.pop()
            p.puzzle.set(p.grid.get(candidate), candidate)
            discarded = discarded & candidate 
    # clear all locations that are part of the puzzle
    p.clearPuzzleCells(emptied)
    discard

proc newPuzzle*(grid: Grid, difficulty: Difficulty = Difficulty.Easy): Puzzle =
    result = Puzzle(difficulty: difficulty)
    result.grid = grid
    result.puzzle = grid
    result.emptiedLocations = @[]

